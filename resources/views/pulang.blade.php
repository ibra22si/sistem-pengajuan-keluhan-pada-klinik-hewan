<!DOCTYPE html>
<html lang="en" class="scroll-smooth group" data-sidebar="brand" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>Halaman Hewan Pulang</title>
        <meta  name="viewport"  content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta  content="Tailwind Multipurpose Admin & Dashboard Template"  name="description"/>
        <meta content="" name="Mannatthemes" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" />
       
        <!-- Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/nice-select2/css/nice-select2.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/swiper/swiper-bundle.min.css">
        <!-- Main Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/icofont/icofont.min.css">
        <link href="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/css/tailwind.min.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/animate.css/animate.min.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/animate.css/animate.min.css">


    </head>
   
    <body data-layout-mode="light"  data-sidebar-size="default" data-theme-layout="vertical" class="bg-[#EEF0FC] dark:bg-gray-900">
        <header class="relative z-40 w-full bg-white print:hidden">
        <body data-layout-mode="light"  data-sidebar-size="default" data-theme-layout="vertical" class="bg-[#EEF0FC] dark:bg-gray-900">
                <div class="container mx-auto">
                  <div class="relative -mx-4 flex items-center justify-between">
                    <div class="w-full max-w-full px-4 lg:w-60">
                      <div class="relative py-4 group categories">
                        <a href="javascript:void(0)" class="inline-flex  relative items-center justify-between whitespace-nowrap rounded bg-brand-50/70 px-5 py-2 text-base font-medium text-brand-500 hover:bg-opacity-90">
                          <span class="pe-2">
                          <img src="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png"  class="h-7 w-10"></i>
                          </span>
                          Klinik Hewan
                        </a>
                        <div class="absolute left-0 top-[100%] z-10 w-[250px] rounded-lg border-[.5px] dark:border-slate-700/40 bg-white py-4   duration-400 group-[.categories]:group-hover:block  hidden">
                          <span class="absolute -top-[6px] left-6 -z-10 hidden h-3 w-3 rotate-45 rounded-sm border-[.5px] border-r-0 border-b-0 dark:border-slate-700/40 bg-white lg:block"></span>
                          <div class="px-6">
                          <a href="{{ route('login') }}" class="flex items-center justify-between rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                              Login Admin
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="flex w-full items-center justify-between px-4">
                      <div class="w-full">
                        <nav id="mobile-menu-2" class="order-2 hidden w-full items-center justify-between md:order-1 md:ms-5 lg:flex md:w-auto">
                        <ul class="flex border-b dark:border-b-slate-700">
                        <li class="mr-1">
                            <a class="bg-white dark:bg-slate-600/20 inline-block py-2 px-4 text-slate-500 dark:text-slate-400 hover:text-primary-600 font-medium" href="/">Beranda</a>
                          </li>
                          <li class="mr-1">
                            <a class="bg-white dark:bg-slate-600/20 inline-block py-2 px-4 text-slate-500 dark:text-slate-400 hover:text-primary-600 font-medium" href="{{ route('pendaftaran.index') }}">Hewan Anda</a>
                          </li>
                          <li class="-mb-px mr-1">
                            <a class="bg-white dark:bg-slate-600/20 inline-block border-b border-primary-500  py-2 px-4 text-primary-600 font-medium" href="HewanPulang">Riwayat Hewan</a>
                          </li>
                        </ul>
                        </nav>
                      </div>
                    </div>            
            </header>
          <div class="ltr:flex flex-1 rtl:flex-row-reverse">
              <div class="page-wrapper relative  duration-300 pt-0 w-full">
                  <div class="xl:w-full  min-h-[calc(100vh-0px)] relative pb-0">
                      <div class="container my-4">
                          <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12 gap-4">  
                              </div><!--end col-->
                              <div class="col-span-12 sm:col-span-8 md:col-span-9 lg:col-span-9 xl:col-span-9">
                                <div class="grid md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12 gap-4 mb-4">
                                    @foreach ($laporan as $laporan)  
                                    <div class="sm:col-span-12 md:col-span-4 lg:col-span-3 xl:col-span-3">
                                        <div class="bg-white dark:bg-gray-900 border border-slate-200 dark:border-slate-700/40 rounded-md w-full relative">
                                            <div class="flex-auto text-center">
                                                <div class="flex-auto text-center bg-gray-100">
                                                @if ($laporan->poli)
                                                    <img src="{{ Storage::url('public/pendaftaran/') . $laporan->poli->keluhan->pendaftaran->foto }}" alt="" class="h-44 inline-block my-4 transition ease-in-out delay-50 hover:-translate-y-1 hover:scale-110 duration-500">
                                                    @elseif ($laporan->emergency)
                                                    <img src="{{ Storage::url('public/pendaftaran/') . $laporan->emergency->keluhan->pendaftaran->foto }}" alt="" class="h-44 inline-block my-4 transition ease-in-out delay-50 hover:-translate-y-1 hover:scale-110 duration-500">
                                                    @endif
                                                </div>
                                                <div class="flex-auto text-center p-4">
                                                <a class="text-xl font-semibold text-slate-500 dark:text-gray-400 leading-3 block mb-2 truncate"> 
                                                    @if ($laporan->poli)
                                                        {{ $laporan->poli->keluhan->pendaftaran->nama_pemilik }}
                                                    @elseif ($laporan->emergency)
                                                        {{ $laporan->emergency->keluhan->pendaftaran->nama_pemilik }}
                                                    @endif</a>
                                                    <a class="text-xl font-semibold text-slate-500 dark:text-gray-400 leading-3 block mb-2 truncate">
                                                    @if ($laporan->poli)
                                                        {{ $laporan->poli->keluhan->pendaftaran->nama_hewan }}
                                                    @elseif ($laporan->emergency)
                                                        {{ $laporan->emergency->keluhan->pendaftaran->nama_hewan }}
                                                    @endif</a>
                                                    <a class="text-xl font-semibold text-slate-500 dark:text-gray-400 leading-5 block mb-2 truncate">
                                                    @if ($laporan->poli)
                                                        {{ $laporan->poli->keluhan->pendaftaran->jenis_hewan }}
                                                    @elseif ($laporan->emergency)
                                                        {{ $laporan->emergency->keluhan->pendaftaran->jenis_hewan }}
                                                    @endif</a><br>
                                                    <form id="delete-form" action="{{ route('HewanPulang.destroy', $laporan->id_laporan) }}" method="post" onsubmit="return executeExample('customPosition')">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="px-4 py-1 lg:px-4 bg-green text-gray-900 text-sm hover:text-white border border-slate-200 font-medium w-full">Ambil Hewan Anda</button>
                                                    </form>
                                                  <button type="button" data-fc-type="modal" data-fc-target="smallmodal_{{ $laporan->id_laporan }}" class="px-4 py-1 lg:px-4 bg-white text-gray-900 text-sm hover:text-black border border-slate-200 font-medium w-full">Detail</button>
                                                  <!-- Small Modal -->
                                                      <div class="modal animate-ModalSlide hidden" id="smallmodal_{{ $laporan->id_laporan }}">
                                                          <div class="relative w-auto pointer-events-none  sm:my-7 sm:mx-auto z-[99] sm:max-w-xs">
                                                              <div class="relative flex flex-col w-full pointer-events-auto bg-white dark:bg-slate-800 bg-clip-padding rounded">
                                                                  <div class="flex shrink-0 items-center justify-between py-2 px-4 rounded-t border-b border-solid dark:border-gray-700 bg-slate-800">
                                                                      <h6 class="mb-0 leading-4 text-base font-semibold text-slate-300 mt-0" id="staticBackdropLabel1">Details</h6>
                                                                      <button type="button" class="box-content w-4 h-4 p-1 bg-slate-700/60 rounded-full text-slate-300 leading-4 text-xl close" aria-label="Close"  data-fc-dismiss>&times;</button>
                                                                  </div>
                                                                  <div class="relative flex-auto p-4 text-slate-600 dark:text-gray-300 leading-relaxed">
                                                                      <p class="font-semibold text-base">Penyakit</p>{{ $laporan->penyakit }}
                                                                  <div class="relative flex-auto p-4 text-slate-600 dark:text-gray-300 leading-relaxed">
                                                                      <p class="font-semibold text-base">Obat</p>{{ $laporan->obat }}
                                                                  <div class="relative flex-auto p-4 text-slate-600 dark:text-gray-300 leading-relaxed">
                                                                      <p class="font-semibold text-base">Jumlah Pembayaran</p>{{ $laporan->pembayaran }}                        
                                                                  </div>
                                                                  <div class="relative flex-auto p-4 text-slate-600 dark:text-gray-300 leading-relaxed">
                                                                      <button class="inline-block focus:outline-none text-red-500 hover:bg-red-500 hover:text-white bg-transparent border border-gray-200 dark:bg-transparent dark:text-red-500 dark:hover:text-white dark:border-gray-700 dark:hover:bg-red-500  text-sm font-medium py-1 px-3 rounded mr-1 close" data-fc-dismiss>Close</button>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div> <!--end card-->
                                    </div>
                                    
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div> <!--end grid-->
                  </div><!--end container-->
                </div>
              </div>
                <!-- footer -->
                    <div class="relative bottom-0 -left-0 -right-0 block print:hidden border-t p-4 bg-black dark:border-slate-700/40">
                        <div class="container">
                          <!-- Footer Start -->
                          <div class="grid  grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4 pt-10">
                            <div class="w-full relative mb-4">                                
                                <div class="flex-auto p-4">
                                    <h5 class="text-xl font-semibold text-slate-300 mb-6">About Us</h5>
                                    <p class="text-slate-500 text-lg" style="width: 550px;px">Klinik hewan dalam dinas peternakan dan kesehatan hewan adalah fasilitas yang
                                      menyediakan pelayanan medis untuk hewan ternak dan hewan peliharaan. Layanan mencakup pemeriksaan kesehatan,
                                      vaksinasi, perawatan penyakit, dan tindakan medis lainnya. Tujuan utamanya adalah menjaga kesehatan hewan, mencegah
                                      penyakit menular.</p>
                                </div><!--end card-body-->
                            </div> <!--end card-->
                            <div class="w-full relative mb-4">                                
                              <div class="flex-auto p-4">
                              </div><!--end card-body-->
                          </div> <!--end card-->
                          <div class="w-full relative mb-4">                                
                              <div class="flex-auto p-4">
                              </div><!--end card-body-->
                          </div> <!--end card-->
                          <div class="w-full relative mb-4">                                
                            <div class="flex-auto p-4">
                              <h5 class="text-xl font-semibold text-slate-300 mb-6 sm:text-center xl:text-left">Contact Us</h5>
                              <div class="mb-5">
                                <p class="text-slate-400 font-semibold">Jl. Pattimura No.2<br>
                                (0761) 44341,22817<br>
                                dinaspkh@riau.go.id
                                </p>
                              </div>
                            </div><!--end card-body-->
                        </div> <!--end card--><!--end card-body-->
                        </div> <!--end card-->
                        </div>
                          <footer
                            class="footer bg-transparent  text-center  font-medium text-slate-400 dark:text-slate-400 md:text-left "
                          >
                            &copy;
                            <script>
                              var year = new Date();document.write(year.getFullYear());
                            </script>
                            Dinas Peternakan Dan Kesehatan Hewan
                          </footer>
                          <!-- end Footer -->
                          <!-- end Footer -->
                        </div>
                      </div>
                </div><!--end main-->
            </div><!--end page-wrapper-->
        </div><!--end div-->
       


        <!-- JAVASCRIPTS -->
        <!-- <div class="menu-overlay"></div> -->
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/lucide/umd/lucide.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/@frostui/tailwindcss/frostui.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/js/pages/validation.init.js"></script>


        <script src="{{asset('AdminLTE')}}/dist/assets/libs/nice-select2/js/nice-select2.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/swiper/swiper-bundle.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/js/app.js"></script>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="{{ asset('AdminLTE') }}/dist/assets/libs/sweetalert2/sweetalert2.all.min.js"></script>
        <script src="{{ asset('AdminLTE') }}/dist/assets/libs/sweetalert2/sweetalert2.all.min.js"></script> 
        <script src="{{ asset('AdminLTE') }}/dist/assets/js/pages/sweetalert.init.js"></script>

        <script>
    function executeExample(position) {
        Swal.fire({
            title: 'Sudah Memenuhi Syarat?',
            text: "Apabila Anda Sudah Melakukan Pembayaran Tekan Tombol OK!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK!',
        }).then(function(result) {
            if (result.isConfirmed) {
                Swal.fire(
                    'Diterima!',
                    'Hewan Anda Sudah Diambil.',
                    'success'
                ).then(function() {
                    // Setelah SweetAlert berhasil, lakukan submit form
                    document.getElementById('delete-form').submit();
                });
            }
        });
        return false; // Pastikan form tidak di-submit secara otomatis
    }
</script>


     
        <script>
    function konfirmasiHapus() {
        if (confirm('Apakah Anda Yakin ?')) {
            var form = document.getElementById('delete-form');
            form.submit();
        }
    }
</script>
        <!-- JAVASCRIPTS -->
    </body>
</html>

