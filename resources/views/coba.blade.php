<!DOCTYPE html>
<html lang="en" class="scroll-smooth group" data-sidebar="brand" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>Robotech - Admin & Dashboard Template</title>
        <meta  name="viewport"  content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta  content="Tailwind Multipurpose Admin & Dashboard Template"  name="description"/>
        <meta content="" name="Mannatthemes" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico" />
        
        <!-- Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/nice-select2/css/nice-select2.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/swiper/swiper-bundle.min.css">
        <!-- Main Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/icofont/icofont.min.css">
        <link href="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/css/tailwind.min.css">

    </head>
    
    <body data-layout-mode="light"  data-sidebar-size="default" data-theme-layout="vertical" class="bg-[#EEF0FC] dark:bg-gray-900">
    
        <!-- leftbar-tab-menu -->
        <header class="relative z-40 w-full bg-white print:hidden">
    <body data-layout-mode="light"  data-sidebar-size="default" data-theme-layout="vertical" class="bg-[#EEF0FC] dark:bg-gray-900">
                      <div
                        class="-left-40 md:left-auto right-0 z-50 my-1 hidden min-w-[300px] sm:min-w-[400px] md:min-w-[400px] lg:min-w-[500px] max-w-full
                        list-none divide-y  divide-gray-100 rounded-md border-slate-700
                        md:border-white text-base shadow dark:divide-gray-600 bg-white
                        dark:bg-slate-800" onclick="event.stopPropagation()">
                        <form class="relative flex w-full items-center rounded-md border dark:border-slate-700/40 bg-[#f4f7ff] ">
                          <div class="relative z-20 border-r border-[#d9d9d9] px-2 hidden lg:block">
                            <select  id="default" class="nice-select border-0 relative z-20 appearance-none bg-transparent ps-2 pe-6 font-medium text-black outline-none">
                            </select>
                          </div>
                          <input type="text" placeholder="I'm shopping for..." class="w-full bg-transparent py-3 ps-6 pe-[58px] text-base font-medium text-body-color outline-none">
                          <a href="javascript:void(0)" class="absolute top-0 right-0 flex h-full w-[52px] items-center justify-center rounded-tr-md rounded-br-md border-s ">
                            <i data-lucide="search" class="w-6 h-6"></i>
                          </a>
                        </form>
                      <div
                        class="left-auto right-0 z-50 my-1 hidden list-none
                        divide-y divide-gray-100 rounded border-slate-700 md:border-white
                        text-base shadow dark:divide-gray-600 bg-white dark:bg-slate-800 w-40"
                        id="navUserdata">
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="container mx-auto">
              <div class="relative -mx-4 flex items-center justify-between">
                <div class="w-full max-w-full px-4 lg:w-60">
                  <div class="relative py-4 group categories">
                    <a href="javascript:void(0)" class="inline-flex  relative items-center justify-between whitespace-nowrap rounded bg-brand-50/70 px-5 py-2 text-base font-medium text-brand-500 hover:bg-opacity-90">
                      <span class="pe-2">
                        <i data-lucide="menu" class="h-5 w-5"></i>
                      </span>
                      All categories
                    </a>

                    <div class="absolute left-0 top-[100%] z-10 w-[250px] rounded-lg border-[.5px] dark:border-slate-700/40 bg-white py-4   duration-400 group-[.categories]:group-hover:block  hidden">
                      <span class="absolute -top-[6px] left-6 -z-10 hidden h-3 w-3 rotate-45 rounded-sm border-[.5px] border-r-0 border-b-0 dark:border-slate-700/40 bg-white lg:block"></span>
              
                      <div class="group submenu relative px-6">
                        <a href="javascript:void(0)" class="flex items-center justify-between rounded py-2 text-sm font-medium text-body-color hover:text-brand group-hover:text-brand">
                          Fashion
                          <span>
                            <i data-lucide="chevron-right" class="h-4"></i>
                          </span>
                        </a>

                        <div  class="left-full top-0 dark:border-slate-700/40 bg-white py-2 group-[.submenu]:group-hover:visible group-[.submenu]:group-hover:opacity-100 lg:invisible lg:absolute lg:w-[600px] lg:rounded lg:border-[.5px] lg:py-8 lg:px-8 lg:opacity-0 xl:w-[650px] block">
                          <div class="-mx-2 flex flex-wrap">
                            <div class="w-full px-2 lg:w-1/3">
                              <div>
                                <h3 class="mb-3 text-base font-semibold text-black uppercase">
                                  Man
                                </h3>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Cargo Pants
                                </a>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Jackets
                                </a>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  T-Shirts
                                </a>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Shirts
                                </a>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Jeans
                                </a>
                              </div>
                            </div>
                            <div class="w-full px-2 lg:w-1/3">
                              <div>
                                <h3 class="mb-3 text-base font-semibold text-black uppercase">
                                  Woman
                                </h3>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Dresses
                                </a>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Tees
                                </a>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Leggings
                                </a>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Wedding Dresses
                                </a>
                                <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                  Prom Dresses
                                </a>
                              </div>
                            </div>
                            <div class="w-full px-2 lg:w-1/3">
                              <h3 class="mb-3 text-base font-semibold text-black uppercase">
                                Kids
                              </h3>
                              <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                Body Wash
                              </a>
                              <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                Nightwear
                              </a>
                              <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                Shorts
                              </a>
                              <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                Sunglasses
                              </a>
                              <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                                Summer Caps
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="px-6">
                        <a href="#" class="flex items-center justify-between rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                          Phone & Smartwatch
                        </a>
                      </div>
                      <div class="px-6">
                        <a href="#" class="flex items-center justify-between rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                          Laptops
                        </a>
                      </div>
                      <div class="px-6">
                        <a href="#" class="block rounded py-2 text-sm font-medium text-body-color hover:text-brand">
                          Jewelry
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="flex w-full items-center justify-between px-4">
                  <div class="w-full">
                    <button data-collapse-toggle="mobile-menu-2" type="button" id="toggle-menu" class=" block ms-auto h-10 w-10 leading-10 border rounded-full  ring-brand focus:ring-1 lg:hidden" aria-controls="mobile-menu-2" aria-expanded="false">
                      <span class="sr-only">Open main menu</span>
                      <i data-lucide="menu" class="w-5 h-5 mx-auto stroke-slate-600 "></i>
                      <i data-lucide="x" class="w-5 h-5 hidden mx-auto stroke-slate-600 "></i>
                    </button>
                    <nav id="mobile-menu-2" class="order-2 hidden w-full items-center justify-between md:order-1 md:ms-5 lg:flex md:w-auto">
                      <ul class="blcok items-center lg:flex px-4 md:px-0">
                        <li>
                          <a href="Home" class="flex justify-between py-2 text-base font-medium text-dark hover:text-brand lg:mx-5 lg:inline-flex lg:py-6 2xl:mx-6">
                            Home
                          </a>
                        </li>
                        <li>
                          <a href="{{ route('pendaftaran.index') }}" class="flex justify-between py-2 text-base font-medium text-dark hover:text-brand lg:mx-5 lg:inline-flex lg:py-6 2xl:mx-6">
                            Hewan Anda
                          </a>
                        </li>
                        <li>
                          <a href="customers-wishlist.html" class="flex justify-between py-2 text-base font-medium text-dark hover:text-brand lg:mx-5 lg:inline-flex lg:py-6 2xl:mx-6">
                            Wishlist
                          </a>
                        </li>
                        <li>
                          <a href="customers-stores.html" class="flex justify-between py-2 text-base font-medium text-dark hover:text-brand lg:mx-5 lg:inline-flex lg:py-6 2xl:mx-6">
                            Stores
                          </a>
                        </li>
                        <li>
                          <a href="customers-checkout.html" class="flex justify-between py-2 text-base font-medium text-dark hover:text-brand lg:mx-5 lg:inline-flex lg:py-6 2xl:mx-6">
                            Checkout
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div>
                <div class="me-2  dropdown relative">
                      <button
                        type="button"
                        class="dropdown-toggle flex items-center rounded-full text-sm
                        focus:bg-none focus:ring-0 dark:focus:ring-0 md:me-0"
                        id="user-profile"
                        aria-expanded="false"
                         data-fc-autoclose="both" data-fc-type="dropdown">
                        <img
                          class="h-8 w-8 rounded-full"
                          src="{{asset('AdminLTE')}}/dist/assets/images/users/avatar-10.png"
                          alt="user photo"
                          />
                      </button>
                      <div
                        class="left-auto right-0 z-50 my-1 hidden list-none
                        divide-y divide-gray-100 rounded border-slate-700 md:border-white
                        text-base shadow dark:divide-gray-600 bg-white dark:bg-slate-800 w-40"
                        id="navUserdata">
                        <ul class="py-1" aria-labelledby="navUserdata">
                          <li>
                            <a
                              href="customers-profile.html"
                              class="flex items-center py-2 px-3 text-sm text-gray-700 hover:bg-gray-50
                              dark:text-gray-200 dark:hover:bg-gray-900/20
                              dark:hover:text-white">
                              <span data-lucide="user"
                                              class="w-4 h-4 inline-block text-slate-800 dark:text-slate-400 me-2"></span>
                              Profile</a>
                          </li>
                          <li>
                            <a
                              href="customers-invoice.html"
                              class="flex items-center py-2 px-3 text-sm text-gray-700 hover:bg-gray-50
                              dark:text-gray-200 dark:hover:bg-gray-900/20
                              dark:hover:text-white">
                              <span data-lucide="file-spreadsheet"
                                              class="w-4 h-4 inline-block text-slate-800 dark:text-slate-400 me-2"></span>
                              Invoice</a>
                          </li>
                          <li>
                            <a
                              href="auth-lock-screen.html"
                              class="flex items-center py-2 px-3 text-sm text-red-400 hover:bg-gray-50 hover:text-red-500
                              dark:text-red-400 dark:hover:bg-gray-900/20
                              dark:hover:text-red-500">
                              <span data-lucide="power"
                                              class="w-4 h-4 inline-block text-red-400 dark:text-red-400 me-2"></span>
                              Sign out</a>
                          </li>
                        </ul>
                      </div>
              </div>
            </div>
          </div>
        </header>
        
        <div class="ltr:flex flex-1 rtl:flex-row-reverse">
            <div class="page-wrapper relative  duration-300 pt-0 w-full">
                <div class="xl:w-full  min-h-[calc(100vh-56px)] relative pb-0"> 
                    <div class="container my-4">
                        <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12 gap-4">  
                            <div class="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-12 xl:col-span-12">
                                <div class="grid grid-cols-1">
                                    <div class="sm:-mx-6 lg:-mx-8">
                                        <div class="relative overflow-x-auto block w-full sm:px-6 lg:px-8">
                                            <table class="w-full">
                                            <thead class="bg-slate-700 dark:bg-slate-900/30">
        <tr>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Foto Hewan
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Nama
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Email
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Alamat  
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Nomor Handphone  
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Nomor KTP  
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Nama Hewan  
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Jenis Hewan  
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Usia Hewan  
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Tanggal Pendaftaran
            </th>
            <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-slate-100 uppercase">
                Action
            </th>
        </tr>
    </thead>
    <tbody>
      @forelse ($data AS $pendaftaran)
      <tr class="bg-white border-b border-dashed dark:bg-gray-900 dark:border-gray-700/40">
            <td class="p-3 text-sm font-medium whitespace-nowrap dark:text-white">
            <img src="{{ Storage::url('public/pendaftaran/') . $pendaftaran->foto }}" class="rounded" style="width: 150px">
            </td>
                                       
                                        <td>{{ $pendaftaran->nama_pemilik }}</td>
                                        <td>{{ $pendaftaran->email }}</td>
                                        <td>{{ $pendaftaran->alamat }}</td>
                                        <td>{{ $pendaftaran->no_hp }}</td>
                                        <td>{{ $pendaftaran->no_ktp }}</td>
                                        <td>{{ $pendaftaran->nama_hewan }}</td>
                                        <td>{{ $pendaftaran->jenis_hewan }}</td>
                                        <td>{{ $pendaftaran->usia_hewan }}</td>
                                        <td>{{ $pendaftaran->tanggal_daftar }}</td>
                                        <td class="text-center">
                                            <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('pendaftaran.destroy', $pendaftaran->id_daftar) }}" method="post">
                                                <a href="{{ route('pendaftaran.edit', $pendaftaran->id_daftar) }}" class="px-2 py-1 lg:px-4 bg-primary  text-white text-sm  rounded-full transition hover:bg-primary-600 border border-primary font-medium">Ajukan Keluhan</a>
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="px-2 py-1 lg:px-4 bg-white text-gray-900 text-sm font-medium  focus:outline-none  rounded-full transition border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10  dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">Hapus Pendaftaran</button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="10">
                                            <div class="alert alert-danger text-center">
                                                Data pendaftaran belum tersedia.
                                            </div>
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                                            </table>
                                        </div><!--end div-->
                                    </div><!--end div-->
                                </div><!--end grid-->
                                <div class="flex justify-between mt-4">
                                    <div class="self-center">
                                        <p class="dark:text-slate-400">Showing 1 - 20 of 30</p>
                                    </div>
                                    <div class="self-center">
                                        <ul class="inline-flex items-center -space-x-px">
                                            <li>
                                                <a href="#" class=" py-2 px-3 ms-0 leading-tight text-gray-500 bg-white rounded-l-lg border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-900 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                                    <i class="icofont-simple-left"></i>
                                                </a>
                                            </li>
                                            <li>
                                            <a href="#" class="py-2 px-3 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-900 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">1</a>
                                            </li>
                                            <li>
                                            <a href="#" aria-current="page" class="z-10 py-2 px-3 leading-tight text-brand-600 bg-brand-50 border border-brand-300 hover:bg-brand-100 hover:text-brand-700 dark:border-gray-700 dark:bg-gray-700 dark:text-white">2</a>
                                            </li>
                                            <li>
                                                <a href="#" class="py-2 px-3 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-900 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">3</a>
                                            </li>
                                            <li>
                                            <a href="#" class=" py-2 px-3 leading-tight text-gray-500 bg-white rounded-r-lg border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-900 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                                <i class="icofont-simple-right"></i>
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
</div>
</div>
</div>
                            </div><!--end col-->                    
                        </div> <!--end grid-->
                    </div><!--end container--> 
                    <!-- footer -->
                    <div class="relative bottom-0 -left-0 -right-0 block print:hidden border-t p-4 bg-black dark:border-slate-700/40">
                        <div class="container">
                          <!-- Footer Start -->
                          <div class="grid  grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4 pt-10">
                            <div class="w-full relative mb-4">                                
                                <div class="flex-auto p-4">
                                    <div class="mb-5">
                                      <a href="customers-home.html">
                                        <img src="assets/images/logo-sm.png" alt="" class="h-8 inline-block me-3">
                                        <img src="assets/images/logo.png" alt="" class="h-8 inline-block">
                                      </a>
                                    </div>
                                    <p class="text-slate-500 text-lg">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                                </div><!--end card-body-->
                            </div> <!--end card-->
                            <div class="w-full relative mb-4">                                
                              <div class="flex-auto p-4">
                                <h5 class="text-xl font-semibold text-slate-300 mb-6">Customers</h5>
                                <ul class="list-none footer-links">
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Home</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Product details</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Cart</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Checkout</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Wishlist</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Invoice</a>
                                  </li>
                                </ul>
                              </div><!--end card-body-->
                          </div> <!--end card-->
                          <div class="w-full relative mb-4">                                
                              <div class="flex-auto p-4">
                                <h5 class="text-xl font-semibold text-slate-300 mb-6">Admin</h5>
                                <ul class="list-none footer-links">
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Dashboard</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Add product</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Orders</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Customers</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Order details</a>
                                  </li>
                                  <li class="mb-2">
                                    <a href="#" class="border-b border-solid border-transparent text-slate-400 hover:border-white hover:text-white">Refund</a>
                                  </li>
                                </ul>
                              </div><!--end card-body-->
                          </div> <!--end card-->
                          <div class="w-full relative mb-4">                                
                            <div class="flex-auto p-4">
                              <h5 class="text-xl font-semibold text-slate-300 mb-6 sm:text-center xl:text-left">Contact Us</h5>
                              <div class="mb-5">
                                <p class="text-slate-400 font-semibold">1884 George Avenue<br>
                                  Mobile, AL 36603
                                </p>
                              </div>
                              <div class="flex sm:justify-center xl:justify-start">
                                <a href="" class="w-8 h-8 leading-7 border-2 border-gray-500 rounded-full text-center duration-300 text-gray-400 hover:text-white hover:bg-blue-600 hover:border-blue-600">
                                  <i class="icofont-facebook"></i>
                                </a>
                                <a href="" class="w-8 h-8 leading-7 border-2 border-gray-500 rounded-full text-center duration-300 ml-2 text-gray-400 hover:text-white hover:bg-blue-400 hover:border-blue-400">
                                  <i class="icofont-twitter"></i>
                                </a>
                                <a href="" class="w-8 h-8 leading-7 border-2 border-gray-500 rounded-full text-center duration-300 ml-2 text-gray-400 hover:text-white hover:bg-red-600 hover:border-red-600">
                                  <i class="icofont-google-plus"></i>
                                </a>
                              </div>
                            </div><!--end card-body-->
                        </div> <!--end card-->
                        </div>
                          <footer
                            class="footer bg-transparent  text-center  font-medium text-slate-400 dark:text-slate-400 md:text-left "
                          >
                            &copy;
                            <script>
                              var year = new Date();document.write(year.getFullYear());
                            </script>
                            Robotech
                            <span class="float-right hidden text-slate-400 dark:text-slate-400 md:inline-block"
                              >Crafted with <i class="ti ti-heart text-red-500"></i> by
                              Mannatthemes</span
                            >
                          </footer>
                          <!-- end Footer -->
                        </div>
                      </div>
  
                </div><!--end main-->
            </div><!--end page-wrapper-->
        </div><!--end div-->
        

        <!-- JAVASCRIPTS -->
        <!-- <div class="menu-overlay"></div> -->
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/lucide/umd/lucide.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/@frostui/tailwindcss/frostui.js"></script>

        <script src="{{asset('AdminLTE')}}/dist/assets/libs/nice-select2/js/nice-select2.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/swiper/swiper-bundle.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/js/app.js"></script>
        <script>
            NiceSelect.bind(document.querySelector(".nice-select"));
            var swiper = new Swiper(".defaultSwiper", {
                autoplay: {
                    delay: 3500,
                    disableOnInteraction: true,
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
            });    
        </script>
        <!-- JAVASCRIPTS -->
    </body>
</html>