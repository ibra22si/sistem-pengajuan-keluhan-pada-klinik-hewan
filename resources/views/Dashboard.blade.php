@auth
    @if(auth()->user()->role === 'user')
        <script>window.location = "{{ url('/') }}";</script>
    @endif
@endauth
<!DOCTYPE html>
<html lang="en" class="scroll-smooth group" data-sidebar="brand" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>Dashboard</title>
        <meta  name="viewport"  content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta  content="Tailwind Multipurpose Admin & Dashboard Template"  name="description"/>
        <meta content="" name="Mannatthemes" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" />
        
        <!-- Css -->
        <!-- Main Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/icofont/icofont.min.css">
        <link href="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/css/tailwind.min.css">

    </head>
    
    <body data-layout-mode="light"  data-sidebar-size="default" data-theme-layout="vertical" class="bg-[#EEF0FC] dark:bg-gray-900">
    
        <!-- leftbar-tab-menu -->
        

        <div class="min-h-full z-[99]  fixed inset-y-0 print:hidden bg-gradient-to-t from-[#6f3dc3] from-10% via-[#603dc3] via-40% to-[#5c3dc3] to-100% dark:bg-[#603dc3] main-sidebar duration-300 group-data-[sidebar=dark]:bg-[#603dc3] group-data-[sidebar=brand]:bg-brand group-[.dark]:group-data-[sidebar=brand]:bg-[#603dc3]">
            <div class=" text-center border-b bg-[#603dc3] border-r h-[64px] flex justify-center items-center brand-logo dark:bg-[#603dc3] dark:border-slate-700/40 group-data-[sidebar=dark]:bg-[#603dc3] group-data-[sidebar=dark]:border-slate-700/40 group-data-[sidebar=brand]:bg-brand group-[.dark]:group-data-[sidebar=brand]:bg-[#603dc3] group-data-[sidebar=brand]:border-slate-700/40">
                
                    <span>
                        <img src="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" alt="logo-small" class="logo-sm h-12 align-middle inline-block">
                    </span>
                    <span>
                    <span class="logo-text-lg text-2xl font-bold inline-block" style="color: #FFFFFF; font-family: 'Arial', sans-serif;">Klinik Hewan</span>
                  </span>
                </a>
            </div>
            <div class="border-r pb-14 h-[100vh] dark:bg-[#603dc3] dark:border-slate-700/40 group-data-[sidebar=dark]:border-slate-700/40 group-data-[sidebar=brand]:border-slate-700/40" data-simplebar>
                <div class="p-4 block">
                    <ul class="navbar-nav">
                            <div id="parent-accordion" data-fc-type="accordion">
                                <a href="Dashboard"
                                   class="nav-link hover:bg-transparent hover:text-black  rounded-md dark:hover:text-slate-200   flex items-center  decoration-0 px-3 py-3 cursor-pointer group-data-[sidebar=dark]:hover:text-slate-200 group-data-[sidebar=brand]:hover:text-slate-200 "
                                   data-fc-type="collapse" data-fc-parent="parent-accordion">
                                    <span data-lucide="home"
                                          class="w-5 h-5 text-center text-slate-800 dark:text-slate-400 me-2 group-data-[sidebar=dark]:text-slate-400 group-data-[sidebar=brand]:text-slate-400"></span>
                                    <span>Dashboard</span>
                                </a>                    
                                </div>
                                <div id="parent-accordion" data-fc-type="accordion">
                                <a href="DataPasien"
                                   class="nav-link hover:bg-transparent hover:text-black  rounded-md dark:hover:text-slate-200   flex items-center  decoration-0 px-3 py-3 cursor-pointer group-data-[sidebar=dark]:hover:text-slate-200 group-data-[sidebar=brand]:hover:text-slate-200 "
                                   data-fc-type="collapse" data-fc-parent="parent-accordion">
                                    <span data-lucide="contact"
                                          class="w-5 h-5 text-center text-slate-800 dark:text-slate-400 me-2 group-data-[sidebar=dark]:text-slate-400 group-data-[sidebar=brand]:text-slate-400"></span>
                                    <span>Data Pasien</span>
                                </a>                    
                                </div>
                                <div id="parent-accordion" data-fc-type="accordion">
                                <a href="keluhan"
                                   class="nav-link hover:bg-transparent hover:text-black  rounded-md dark:hover:text-slate-200   flex items-center  decoration-0 px-3 py-3 cursor-pointer group-data-[sidebar=dark]:hover:text-slate-200 group-data-[sidebar=brand]:hover:text-slate-200 "
                                   data-fc-type="collapse" data-fc-parent="parent-accordion">
                                    <span data-lucide="app-window"
                                          class="w-5 h-5 text-center text-slate-800 dark:text-slate-400 me-2 group-data-[sidebar=dark]:text-slate-400 group-data-[sidebar=brand]:text-slate-400"></span>
                                    <span>Keluhan</span>
                                </a>                    
                                </div>
                                <div id="parent-accordion" data-fc-type="accordion">
                                <a href="poli"
                                   class="nav-link hover:bg-transparent hover:text-black  rounded-md dark:hover:text-slate-200   flex items-center  decoration-0 px-3 py-3 cursor-pointer group-data-[sidebar=dark]:hover:text-slate-200 group-data-[sidebar=brand]:hover:text-slate-200 "
                                   data-fc-type="collapse" data-fc-parent="parent-accordion">
                                    <span data-lucide="activity-square"
                                          class="w-5 h-5 text-center text-slate-800 dark:text-slate-400 me-2 group-data-[sidebar=dark]:text-slate-400 group-data-[sidebar=brand]:text-slate-400"></span>
                                    <span>Pasien Poli</span>
                                </a>                    
                                </div>
                                <div id="parent-accordion" data-fc-type="accordion">
                                <a href="emergency"
                                   class="nav-link hover:bg-transparent hover:text-black  rounded-md dark:hover:text-slate-200   flex items-center  decoration-0 px-3 py-3 cursor-pointer group-data-[sidebar=dark]:hover:text-slate-200 group-data-[sidebar=brand]:hover:text-slate-200 "
                                   data-fc-type="collapse" data-fc-parent="parent-accordion">
                                    <span data-lucide="bed"
                                          class="w-5 h-5 text-center text-slate-800 dark:text-slate-400 me-2 group-data-[sidebar=dark]:text-slate-400 group-data-[sidebar=brand]:text-slate-400"></span>
                                    <span>Pasien Rawat Inap</span>
                                </a>                    
                                </div>
                                <div id="parent-accordion" data-fc-type="accordion">
                                <a href="laporan"
                                   class="nav-link hover:bg-transparent hover:text-black  rounded-md dark:hover:text-slate-200   flex items-center  decoration-0 px-3 py-3 cursor-pointer group-data-[sidebar=dark]:hover:text-slate-200 group-data-[sidebar=brand]:hover:text-slate-200 "
                                   data-fc-type="collapse" data-fc-parent="parent-accordion">
                                    <span data-lucide="check-circle-2"
                                          class="w-5 h-5 text-center text-slate-800 dark:text-slate-400 me-2 group-data-[sidebar=dark]:text-slate-400 group-data-[sidebar=brand]:text-slate-400"></span>
                                    <span>Riwayat Hewan</span>
                                </a>                    
                                </div>
                              </div>
                          </div>
                      </div>

        
                      <nav id="topbar" class="topbar border-b  dark:border-slate-700/40  fixed inset-x-0  duration-300
             block print:hidden z-50">
            <div class="mx-0 flex max-w-full flex-wrap items-center lg:mx-auto relative top-[50%] start-[50%] transform -translate-x-1/2 -translate-y-1/2">
              <div class="ltr:mx-2  rtl:mx-2">
                <button id="toggle-menu-hide" class="button-menu-mobile flex rounded-full md:me-0 relative">
                  <!-- <i class="ti ti-chevrons-left text-3xl  top-icon"></i> -->
                  <i data-lucide="menu" class="top-icon w-5 h-5"></i>
                </button>
              </div>
              <div class="flex items-center md:w-[40%] lg:w-[30%] xl:w-[20%]">
                <div class="relative ltr:mx-2 rtl:mx-2 self-center">
                  <button class="px-2 py-1 bg-primary-500/10 border border-transparent collapse:bg-green-100 text-primary text-sm rounded hover:bg-blue-600 hover:text-white"><i class="ti ti-plus me-1"></i> Halaman Dashboard</button>
                </div>
              </div>
      
              <div class="order-1 ltr:ms-auto rtl:ms-0 rtl:me-auto flex items-center md:order-2">
                <div class="ltr:me-2 ltr:md:me-4 rtl:me-0 rtl:ms-2 rtl:lg:me-0 rtl:md:ms-4 dropdown relative">
                  <div
                    class="left-auto right-0 z-50 my-1 hidden min-w-[300px]
                    list-none divide-y  divide-gray-100 rounded-md border-slate-700
                    md:border-white text-base shadow dark:divide-gray-600 bg-white
                    dark:bg-slate-800" onclick="event.stopPropagation()">
                    <div class="relative">
                    </div>
                  </div>
                </div>
                <div class="ltr:me-2 ltr:md:me-4 rtl:me-0 rtl:ms-2 rtl:lg:me-0 rtl:md:ms-4">

                  <button id="toggle-theme" class="flex rounded-full md:me-0 relative">
                    <span data-lucide="moon" class="top-icon w-5 h-5 light "></span>
                    <span data-lucide="sun" class="top-icon w-5 h-5 dark hidden"></span>
                  </button>
                </div>
                
                <div class="me-2  dropdown relative">
                  <button
                    type="button"
                    class="dropdown-toggle flex items-center rounded-full text-sm
                    focus:bg-none focus:ring-0 dark:focus:ring-0 md:me-0"
                    id="user-profile"
                    aria-expanded="false"
                     data-fc-autoclose="both" data-fc-type="dropdown">

                     <i class="fa fa-user mr-1"></i>
                    <span class="ltr:ms-2 rtl:ms-0 rtl:me-2 hidden text-left xl:block">
                      <span class="-mt-0.5 block text-xs text-slate-500 dark:text-gray-400">{{ auth()->user()->name }}</span>
                    </span>
                  </button>
                  <div
                    class="left-auto right-0 z-50 my-1 hidden list-none
                    divide-y divide-gray-100 rounded border border-slate-700/10
                    text-base shadow dark:divide-gray-600 bg-white dark:bg-slate-800 w-40"
                    id="navUserdata">
            
                    <ul class="py-1" aria-labelledby="navUserdata">
                      <li>
                        <a
                        href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                          class="flex items-center py-2 px-3 text-sm text-red-500 hover:bg-gray-50 hover:text-red-600
                          dark:text-red-500 dark:hover:bg-gray-900/20
                          dark:hover:text-red-500">
                          <span data-lucide="power"
                                          class="w-4 h-4 inline-block text-red-500 dark:text-red-500 me-2"></span>
                          Sign out</a>
                      </li>
                      <li>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>


        <div class="ltr:flex flex-1 rtl:flex-row-reverse">
            <div class="page-wrapper relative ltr:ml-auto rtl:mr-auto rtl:ml-0 w-[calc(100%-260px)] px-4 pt-[64px] duration-300">
                <div class="xl:w-full">        
                    <div class="flex flex-wrap">
                        <div class="flex items-center py-4 w-full">
                            <div class="w-full">                    
                                <div class="">
                                    <div class="flex flex-wrap justify-between">
                                        <div class="items-center ">
                                            <h1 class="font-medium text-3xl block dark:text-slate-100">Selamat Datang Admin</h1>
                                        </div>
                                        <div class="flex items-center">
                                            <div class="today-date leading-5 mt-2 lg:mt-0 form-input w-auto rounded-md border inline-block border-primary-500/60 dark:border-primary-500/60 text-primary-500 bg-transparent px-3 py-1 focus:outline-none focus:ring-0 placeholder:text-slate-400/70 placeholder:font-normal placeholder:text-sm hover:border-primary-400 focus:border-primary-500 dark:focus:border-primary-500  dark:hover:border-slate-700">
                                                <input type="text" class="dash_date border-0 focus:border-0 focus:outline-none" readonly  required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end container-->
                <div class="xl:w-full  min-h-[calc(100vh-138px)] relative pb-14"> 
                    <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12 gap-4">  
                        <div class="col-span-12 sm:col-span-8 md:col-span-6 lg:col-span-3 xl:col-span-3">
                            <div class="bg-white shadow-sm dark:shadow-slate-700/10 dark:bg-gray-900  rounded-md w-full relative mb-4">
                                <div class="flex-auto p-4">
                                    <div class="flex justify-between xl:gap-x-2 items-cente">
                                        <div class="self-center"> 
                                            <p class="text-gray-800 font-semibold dark:text-slate-400 text-lg uppercase">Pasien Terdaftar</p>                           
                                            <h3 class="my-4 font-semibold text-[30px] dark:text-slate-200"> {{ $data[0]->Terdaftar }}</h3>                                                
                                        </div>                                       
                                        <div class="self-center">                                                
                                            <i data-lucide="user-check" class=" h-16 w-16 stroke-primary-500/30"></i>
                                        </div>                                            
                                    </div>
                                    <p class="truncate text-slate-400"><span class="text-green-500"><i class="mdi mdi-trending-up"></i></span>Daftar Pasien Klinik Hewan</p>
                                </div><!--end card-body-->  
                                <div class="flex-auto p-0 overflow-hidden"> 
                                    <div class="flex mb-0 h-full">
                                        <div class="w-full">                                                
                                            <div class="apexchart-wrapper">
                                                </div>
                                        </div>
                                    </div>
                                </div><!--end card-body-->                             
                            </div> <!--end inner-grid--> 
                        </div><!--end col-->
                        <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-3 xl:col-span-3">
                            <div class="bg-white shadow-sm dark:shadow-slate-700/10 dark:bg-gray-900  rounded-md w-full relative mb-4">
                                <div class="flex-auto p-4">
                                    <div class="flex justify-between xl:gap-x-2 items-cente">
                                        <div class="self-center"> 
                                            <p class="text-gray-800 font-semibold dark:text-slate-400 uppercase">Hewan Poli</p>                           
                                            <h3 class="my-4 font-semibold text-[30px] dark:text-slate-200">{{ $data2[0]->poli }}</h3>                                              
                                        </div>                                       
                                        <div class="self-center">                                                
                                            <i data-lucide="activity" class=" h-16 w-16 stroke-green/30"></i>
                                        </div>                                            
                                    </div>
                                    <p class="truncate text-slate-400"><span class="text-red-500"><i class="mdi mdi-trending-down"></i></span>Hewan Pada Tahap Pemulihan</p>
                                </div><!--end card-body-->   
                                <div class="flex-auto p-0 overflow-hidden"> 
                                    <div class="flex mb-0 h-full">
                                        <div class="w-full">                                                
                                            
                                        </div>
                                    </div>
                                </div><!--end card-body-->                              
                            </div> <!--end inner-grid-->                                                                                                  
                        </div><!--end col-->
                        <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-3 xl:col-span-3">
                            <div class="bg-white shadow-sm dark:shadow-slate-700/10 dark:bg-gray-900  rounded-md w-full relative mb-4">
                                <div class="flex-auto p-4">
                                    <div class="flex justify-between xl:gap-x-2 items-cente">
                                        <div class="self-center"> 
                                            <p class="text-gray-800 font-semibold dark:text-slate-400 uppercase">Hewan Rawat Inap</p>                           
                                            <h3 class="my-4 font-semibold text-[30px] dark:text-slate-200">{{ $data3[0]->emergency }}</h3>                                              
                                        </div>                                       
                                        <div class="self-center">                                                
                                            <i data-lucide="bed" class=" h-16 w-16 stroke-green/30"></i>
                                        </div>                                            
                                    </div>
                                    <p class="truncate text-slate-400"><span class="text-red-500"><i class="mdi mdi-trending-down"></i></span>Hewan Pada Tahap Pemulihan</p>
                                </div><!--end card-body-->   
                                <div class="flex-auto p-0 overflow-hidden"> 
                                    <div class="flex mb-0 h-full">
                                        <div class="w-full">                                                
                                            
                                        </div>
                                    </div>
                                </div><!--end card-body-->                              
                            </div> <!--end inner-grid-->                                                                                                  
                        </div><!--end col-->
                        <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-3 xl:col-span-3">
                            <div class="bg-white shadow-sm dark:shadow-slate-700/10 dark:bg-gray-900  rounded-md w-full relative mb-4">
                                <div class="flex-auto p-4">
                                    <div class="flex justify-between xl:gap-x-2 items-cente">
                                        <div class="self-center"> 
                                            <p class="text-gray-800 font-semibold dark:text-slate-400 uppercase">Hewan pulang</p>                           
                                            <h3 class="my-4 font-semibold text-[30px] dark:text-slate-200">{{ $data4[0]->Pulang }}</h3>                                                
                                        </div>                                       
                                        <div class="self-center">                                                
                                            <i data-lucide="check-circle" class=" h-16 w-16 stroke-yellow-500/30"></i>
                                        </div>                                            
                                    </div>
                                    <p class="truncate text-slate-400"><span class="text-green-500"><i class="mdi mdi-trending-up"></i></span>Hewan Sudah Boleh Pulang</p>        
                                </div><!--end card-body-->  
                                <div class="flex-auto p-0 overflow-hidden"> 
                                    <div class="grid grid-cols-12">  
                                    </div><!--end grid-->
                                </div><!--end card-body-->                                
                            </div> <!--end inner-grid--> 
                        
                    
                     
                    <!-- footer -->
                    <div class="absolute bottom-0 -left-4 -right-4 block print:hidden border-t p-4 h-[52px] dark:border-slate-700/40">
                        <div class="container">
                          <!-- Footer Start -->
                          <footer
                            class="footer bg-transparent  text-center  font-medium text-slate-600 dark:text-slate-400 md:text-left "
                          >
                            &copy;
                            <script>
                              var year = new Date();document.write(year.getFullYear());
                            </script>
                            Robotech
                            <span class="float-right hidden text-slate-600 dark:text-slate-400 md:inline-block"
                              >Crafted with <i class="ti ti-heart text-red-500"></i> by
                              Mannatthemes</span
                            >
                          </footer>
                          <!-- end Footer -->
                        </div>
                      </div>
  
  
                </div><!--end container-->
            </div>
        </div>

        <!-- JAVASCRIPTS -->
        <!-- <div class="menu-overlay"></div> -->
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/lucide/umd/lucide.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/@frostui/tailwindcss/frostui.js"></script>

        <script src="{{asset('AdminLTE')}}/dist/assets/libs/apexcharts/apexcharts.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/js/pages/analytics-index.init.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/js/app.js"></script>
        <!-- JAVASCRIPTS -->
    </body>
</html>