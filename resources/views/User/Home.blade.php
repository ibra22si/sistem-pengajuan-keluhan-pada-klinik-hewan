<!DOCTYPE html>
<html lang="en" class="scroll-smooth group" data-sidebar="brand" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>Halaman Utama</title>
        <meta  name="viewport"  content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta  content="Tailwind Multipurpose Admin & Dashboard Template"  name="description"/>
        <meta content="" name="Mannatthemes" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" />
        
        <!-- Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/nice-select2/css/nice-select2.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/swiper/swiper-bundle.min.css">
        <!-- Main Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/icofont/icofont.min.css">
        <link href="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/css/tailwind.min.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/animate.css/animate.min.css">

    </head>
    <header class="relative z-40 w-full bg-white print:hidden">
    <body data-layout-mode="light"  data-sidebar-size="default" data-theme-layout="vertical" class="bg-[#EEF0FC] dark:bg-gray-900">
            <div class="container mx-auto">
              <div class="relative -mx-4 flex items-center justify-between">
                <div class="w-full max-w-full px-4 lg:w-60">
                  <div class="relative py-4 group categories">
                    <a href="javascript:void(0)" class="inline-flex  relative items-center justify-between whitespace-nowrap rounded bg-brand-50/70 px-5 py-2 text-base font-medium text-brand-500 hover:bg-opacity-90">
                      <span class="pe-2">
                      <img src="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png"  class="h-7 w-10"></i>
                      </span>
                      Klinik Hewan
                    </a>
                    <div class="absolute left-0 top-[100%] z-10 w-[250px] rounded-lg border-[.5px] dark:border-slate-700/40 bg-white py-4   duration-400 group-[.categories]:group-hover:block  hidden">
                      <span class="absolute -top-[6px] left-6 -z-10 hidden h-3 w-3 rotate-45 rounded-sm border-[.5px] border-r-0 border-b-0 dark:border-slate-700/40 bg-white lg:block"></span>
                      <div class="px-6">
                      @if(Auth::check())
    <form method="POST" action="{{ route('logout') }}">
        @csrf
        <button type="submit" class="flex items-center justify-between rounded py-2 text-sm font-medium text-body-color hover:text-brand">
            Logout
        </button>
    </form>
@else
    <a href="{{ route('login') }}" class="flex items-center justify-between rounded py-2 text-sm font-medium text-body-color hover:text-brand">
        Login Admin
    </a>
@endif

                      </div>
                    </div>
                  </div>
                </div>
                <div class="flex w-full items-center justify-between px-4">
                  <div class="w-full">
                    <nav id="mobile-menu-2" class="order-2 hidden w-full items-center justify-between md:order-1 md:ms-5 lg:flex md:w-auto">
                    <ul class="flex border-b dark:border-b-slate-700">
                      <li class="-mb-px mr-1">
                        <a class="bg-white dark:bg-slate-600/20 inline-block border-b border-primary-500  py-2 px-4 text-primary-600 font-medium" href="/">Beranda</a>
                      </li>
                      @if(Auth::check())
                      <li class="mr-1">
                        <a class="bg-white dark:bg-slate-600/20 inline-block py-2 px-4 text-slate-500 dark:text-slate-400 hover:text-primary-600 font-medium" href="{{ route('pendaftaran.index') }}">Hewan Anda</a>
                      </li>
                      <li class="mr-1">
                        <a class="bg-white dark:bg-slate-600/20 inline-block py-2 px-4 text-slate-500 dark:text-slate-400 hover:text-primary-600 font-medium" href="HewanPulang">Riwayat Hewan</a>
                      </li>
                      @endif
                    </ul>
                    </nav>
                  </div>
                </div>            
        </header>
        <div class="ltr:flex flex-1 rtl:flex-row-reverse">
            <div class="page-wrapper relative  duration-300 pt-0 w-full">
                <div class="xl:w-full  min-h-[calc(100vh-0px)] relative pb-0"> 
                    <div class="bg-soft-gradient">
                        <div class="container">
                            <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12">  
                                <div class="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-12 xl:col-span-12">
                                    <div class="bg- shadow-sm dark:shadow-slate-700/10 dark:bg-gray-900  rounded-md w-full relative">
                                        <div class="flex-auto p-4 pb-0">
                                            <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12">  
                                                <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-6 xl:col-span-6 self-center">
                                                    <div class="flex-auto p-4">
                                                    <div>
                                                        <h1 class="text-[40px] md:text-[50px] lg:text-[70px] leading-[80px] mb-10 font-normal">Selamat Datang Di<span class=" weight-animation  text-transparent text-5xl md:text-5xl lg:text-8xl bg-clip-text bg-gradient-to-r from-purple-500 to-pink-600"> Klinik Hewan</span></h1>
                                                        <p class="text-2xl text-slate-800 mb-10">Tentang Kesehatan Hewan Anda, Kami Berkomitmen Untuk Menjadi Prioritas Utama! Ayo, Segera Daftarkan Hewan Kesayangan Anda!</p>
                                                        @if(Auth::check())
                                                          <button type="button" data-fc-type="modal" data-fc-target="largemodal" class="px-10 py-5 lg:px-6 bg-transparent text-primary text-lg rounded-full transition hover:bg-primary-500 hover:text-white border border-primary font-medium mb-2">
                                                              Daftarkan Hewan Anda
                                                          </button>
                                                          @else
                                                          <a href="{{ route('google.login') }}" class="px-10 py-5 lg:px-10 bg-transparent text-primary text-lg rounded-full transition hover:bg-primary-500 hover:text-white border border-primary font-medium mb-2">
                                                            Login
                                                        </a>
                                                    @endif
                                                      </div>
                                                    <!-- Large Modal -->
                                                      <div class="modal animate-ModalSlide hidden" id="largemodal">
                                                        <div class="relative w-auto pointer-events-none  sm:my-7 sm:mx-auto z-[99] lg:max-w-4xl">
                                                            <div class="relative flex flex-col w-full pointer-events-auto bg-white dark:bg-slate-800 bg-clip-padding rounded">
                                                                <div class="flex shrink-0 items-center justify-between py-2 px-4 rounded-t border-b border-solid dark:border-gray-700 bg-slate-800">
                                                                    <h6 class="mb-0 leading-4 text-base font-semibold text-slate-300 mt-0" id="staticBackdropLabel1">Daftarkan Hewan Anda</h6>
                                                                    <button type="button" class="box-content w-4 h-4 p-1 bg-slate-700/60 rounded-full text-slate-300 leading-4 text-xl close" aria-label="Close" data-fc-dismiss>&times;</button>
                                                                </div>
                                                                  <form action="{{ route('pendaftaran.store') }}"method="post" enctype="multipart/form-data">
                                                                  @csrf
                                                                <div class="relative flex-auto p-4 text-slate-600 dark:text-gray-300 leading-relaxed">
                                                                  <div class="relative z-0 mb-2 w-full group">
                                                                  <label for="foto" class="absolute text-xl text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Masukkan Foto Hewan Anda</label>
                                                                  <input type="file" name="foto" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                      @error('foto')
                                                                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                                                                      @enderror
                                                                    </div>
                                                                <div class="grid xl:grid-cols-2 xl:gap-6">
                                                                  <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="text" name="nama_pemilik" id="nama_pemilik" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="nama_pemilik" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nama Pemilik</label>
                                                                </div>
                                                              <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="text" name="nama_hewan" id="nama_hewan" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="nama_hewan" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nama Hewan</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="text" name="email" id="email" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="email" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Email</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="text" name="jenis_hewan" id="jenis_hewan" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="jenis_hewan" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Jenis Hewan</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="text" name="alamat" id="alamat" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="alamat" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Alamat</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="int" name="usia_hewan" id="usia_hewan" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="usia_hewan" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Usia Hewan</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="int" name="no_hp" id="no_hp" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="no_hp" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nomor Handphone</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="int" name="no_ktp" id="no_ktp" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="no_ktp" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nomor KTP</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                  <select name="jenis_kelamin" id="jenis_kelamin" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" required>
                                                                    <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                                                    <option value="jantan">Jantan</option>
                                                                    <option value="betina">Betina</option>
                                                                  </select>
                                                                  <label for="jenis_kelamin" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Jenis Kelamin Hewan</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                  <input type="date" name="tanggal_daftar" id="tanggal_daftar" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-slate-300/60 appearance-none dark:text-slate-300 dark:border-slate-700 dark:focus:border-primary-500 focus:outline-none focus:ring-0 focus:border-primary-500 peer" placeholder=" " required />
                                                                  <label for="tanggal_daftar" class="absolute text-sm text-gray-400 dark:text-slate-400/70 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-primary-500 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Tanggal Pendaftaran</label>
                                                                </div>
                                                                <div class="relative z-0 mb-2 w-full group">
                                                                </div>
                                                                <div class="flex flex-wrap shrink-0 justify-end p-3  rounded-b border-t border-dashed dark:border-gray-700">
                                                                <button type="reset" class="inline-block focus:outline-none text-red-500 hover:bg-red-500 hover:text-white bg-transparent border border-gray-200 dark:bg-transparent dark:text-red-500 dark:hover:text-white dark:border-gray-700 dark:hover:bg-red-500  text-sm font-medium py-1 px-3 rounded mr-1 close">Reset</button>
                                                                <button onclick="executeExample('success')" class="inline-block focus:outline-none text-primary-500 hover:bg-primary-500 hover:text-white bg-transparent border border-gray-200 dark:bg-transparent dark:text-primary-500 dark:hover:text-white dark:border-gray-700 dark:hover:bg-primary-500  text-sm font-medium py-1 px-3 rounded">Save</button>
                                                                </div>
                                                              </form>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div> 
                                                    </div><!--end card-body-->
                                                </div><!--end col-->     
                                                <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-6 xl:col-span-6">
                                                    <div class="flex-auto p-4 pb-0">
                                                        <div class="swiper defaultSwiper">
                                                            <div class="swiper-wrapper">
                                                                <div class="swiper-slide">
                                                                        <img src="https://www.petconsultoria.com.br/wp-content/uploads/2018/08/pets-header.png" alt="" class="w-full h-auto p-10">
                                                                </div>                    
                                                            </div>
                                                        </div>    
                                                    </div><!--end card-body-->
                                                </div><!--end col-->                                                                      
                                            </div> <!--end grid-->
                                        </div><!--end card-body-->                         
                                    </div> <!--end inner-grid--> 
                                </div><!--end col-->                     
                            </div> <!--end grid-->
                        </div><!--end container-->   
                    </div><!--end section-->
                    <div class="py-5">
                        <div class="container">
                            <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12 gap-4">  
                                <div class="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6 xl:col-span-6">
                                    <div class="bg-white shadow-sm dark:shadow-slate-700/10 dark:bg-gray-900  rounded-md w-full relative overflow-hidden" >
                                        <div class="absolute end-36 z-0 hidden md:block">
                                            <img src="{{asset('AdminLTE')}}/dist/assets/images/products/p-1.png" alt="">
                                        </div>
                                        <div class="flex-auto p-4">
                                            <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12">                                                   
                                                <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-8 xl:col-span-8 self-center">
                                                    <div class="flex-auto p-4 pb-0">
                                                        <h4 class="text-4xl text-slate-800 font-semibold mb-0 font-spartan">Komitmen</h4>
                                                        <p class="text-slate-600 text-2xl mb-3 font-spartan">Kami dari klinik hewan berkomitmen untuk menjaga dan merawat kesehatan hewan peliharaan Anda. Sudah menjadi prioritas kami menjaga setiap hewan yang datang ke kami dan mendapatkan perawatan yang cermat dan penyembuhan yang penuh perhatian</p><br>
                                                    </div><!--end card-body-->
                                                </div><!--end col-->
                                                <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-4 self-center">
                                                    <div class="flex-auto p-4 ">
                                                        <img src="https://static.wixstatic.com/media/84770f_cc7fbf222d044cf09028f921a0cfe36e~mv2.png/v1/fill/w_1163,h_699,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/shutterstock_184908566%20copy.png" alt="" class="w-full h-auto z-[1] relative"> 
                                                    </div><!--end card-body-->
                                                </div><!--end col-->                                                                         
                                            </div> <!--end grid-->
                                        </div><!--end card-body-->                         
                                    </div> <!--end inner-grid--> 
                                </div><!--end col--> 
                                <div class="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6 xl:col-span-6">
                                    <div class="bg-white shadow-sm dark:shadow-slate-700/10 dark:bg-gray-900  rounded-md w-full relative overflow-hidden" >
                                        <div class="absolute end-0 bottom-0 z-0">
                                            <img src="{{asset('AdminLTE')}}/dist/assets/images/products/p-2.png" alt="">
                                        </div>
                                        <div class="flex-auto p-4">
                                            <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12">                                                   
                                                <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-8 xl:col-span-8 self-center">
                                                    <div class="flex-auto p-4 pb-0">
                                                        <h4 class="text-4xl text-slate-800 font-semibold mb-0 font-spartan">Dinas Peternakan Dan Kesehatan Hewan</h4>
                                                        <p class="text-slate-600 text-2xl mb-3 font-spartan">Kesejahteraan hewan adalah prioritas dinas peternakan dan kesehatan adalah melindungi, merawat, dan memastikan kondisi baik bagi semua hewan.</p><br><br>
                                                    </div><!--end card-body-->
                                                </div><!--end col-->
                                                <div class="col-span-12 sm:col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-4 self-center">
                                                    <div class="flex-auto p-4 ">
                                                        <img src="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" alt="" class="w-full h-auto z-[1] relative"> 
                                                    </div><!--end card-body-->
                                                </div><!--end col-->                                                                         
                                            </div> <!--end grid-->
                                        </div><!--end card-body-->                         
                                    </div> <!--end inner-grid--> 
                                </div><!--end col-->   
                                <div class="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-12 xl:col-span-12">
                                    <div class="w-full relative">
                                        <div class="flex-auto p-4">
                                            <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12">                                                   
                                                <div class="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-8 xl:col-span-8 self-center">
                                                    <div class="flex-auto p-4 ">
                                                        <div class="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12">                                                                          
                                                        </div> <!--end grid-->
                                                    </div><!--end card-body-->
                                                </div><!--end col-->                                                                         
                                            </div> <!--end grid-->
                                        </div><!--end card-body-->                         
                                    </div> <!--end inner-grid--> 
                                </div><!--end col-->                   
                            </div> <!--end grid-->                 
                            </div><!--end inner-grid--> 
                        </div><!--end container-->   
                    </div><!--end section-->
                    <div class="pb-5 pt-0 md:py-5">
                        <div class="container">                            
                            <div class=" md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12 gap-4 mb-4 hidden md:grid">                                  
                                <div class="sm:col-span-12  md:col-span-12 lg:col-span-12 xl:col-span-12 ">
                              </div><!--end card-body--> 
                                    </div><!--end card-->  
                                </div><!--end col-->                                
                            </div><!--end inner-grid--> 
                            <div class="grid  grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4 pt-0 md:pt-10">
                            </div><!--end grid-->
                        </div><!--end container-->   
                    </div><!--end section-->
                    <!-- footer -->
                    <div class="relative bottom-0 -left-0 -right-0 block print:hidden border-t p-4 bg-black dark:border-slate-700/40">
                        <div class="container">
                          <!-- Footer Start -->
                          <div class="grid  grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4 pt-10">
                            <div class="w-full relative mb-4">                                
                                <div class="flex-auto p-4">
                                    <h5 class="text-xl font-semibold text-slate-300 mb-6">About Us</h5>
                                    <p class="text-slate-500 text-lg" style="width: 550px;px">Klinik hewan dalam dinas peternakan dan kesehatan hewan adalah fasilitas yang 
                                      menyediakan pelayanan medis untuk hewan ternak dan hewan peliharaan. Layanan mencakup pemeriksaan kesehatan, 
                                      vaksinasi, perawatan penyakit, dan tindakan medis lainnya. Tujuan utamanya adalah menjaga kesehatan hewan, mencegah 
                                      penyakit menular.</p>
                                </div><!--end card-body-->
                            </div> <!--end card-->
                            <div class="w-full relative mb-4">                                
                              <div class="flex-auto p-4">
                              </div><!--end card-body-->
                          </div> <!--end card-->
                          <div class="w-full relative mb-4">                                
                              <div class="flex-auto p-4">
                              </div><!--end card-body-->
                          </div> <!--end card-->
                          <div class="w-full relative mb-4">                                
                            <div class="flex-auto p-4">
                              <h5 class="text-xl font-semibold text-slate-300 mb-6 sm:text-center xl:text-left">Contact Us</h5>
                              <div class="mb-5">
                                <p class="text-slate-400 font-semibold">Jl. Pattimura No.2<br>
                                (0761) 44341,22817<br>
                                dinaspkh@riau.go.id
                                </p>
                              </div>
                             
                            </div><!--end card-body-->
                        </div> <!--end card-->
                        </div>
                          <footer
                            class="footer bg-transparent  text-center  font-medium text-slate-400 dark:text-slate-400 md:text-left "
                          >
                            &copy;
                            <script>
                              var year = new Date();document.write(year.getFullYear());
                            </script>
                            Dinas Peternakan Dan Kesehatan Hewan
                          </footer>
                          <!-- end Footer -->
                        </div>
                      </div>
                </div><!--end main-->
            </div><!--end page-wrapper-->
        </div><!--end div-->
        

        <!-- JAVASCRIPTS -->
        <!-- <div class="menu-overlay"></div> -->
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/lucide/umd/lucide.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/@frostui/tailwindcss/frostui.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/sweetalert2/sweetalert2.all.min.js"></script> 
        <script src="{{asset('AdminLTE')}}/dist/assets/js/pages/sweetalert.init.js"></script>


        <script src="{{asset('AdminLTE')}}/dist/assets/libs/nice-select2/js/nice-select2.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/libs/swiper/swiper-bundle.min.js"></script>
        <script src="{{asset('AdminLTE')}}/dist/assets/js/app.js"></script>
        <script>
            NiceSelect.bind(document.querySelector(".nice-select"));
            var swiper = new Swiper(".defaultSwiper", {
                autoplay: {
                    delay: 3500,
                    disableOnInteraction: true,
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
            });    
        </script>
        <!-- JAVASCRIPTS -->
    </body>
</html>