<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tambah Data Admin</title>
    <!-- Include Tailwind CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body class="bg-gray-300">
    <div class="container mx-auto mt-5 mb-5 lg:w-1/2 xl:w-1/3 mx-auto">
        @extends('app')

        @section('content')
        <div class="w-full  m-auto bg-white dark:bg-slate-800/60 rounded shadow-lg ring-2 ring-slate-300/50 dark:ring-slate-700/50 lg:max-w-md">
            <div class="text-center p-6 bg-slate-900 rounded-t">
                <h2 class="text-2xl font-bold text-gray-800">Register Admin</h2>
            </div>

            @if($errors->any())
                @foreach($errors->all() as $err)
                    <p class="alert alert-danger">{{ $err }}</p>
                @endforeach
            @endif

            <form action="{{ route('register.action') }}" method="POST">
                @csrf
                <div class="mb-4">
                    <label for="Name" class="block text-gray-700 text-sm font-bold mb-2">Name</label>
                    <input class="w-full border border-gray-300 px-3 py-2 rounded focus:outline-none focus:border-indigo-500" type="text" name="name" value="{{ old('name') }}" />
                </div>
                <div class="mb-3">
                    <label class="block text-gray-700 text-sm font-bold mb-2">Username <span class="text-danger">*</span></label>
                    <input class="w-full border border-gray-300 px-3 py-2 rounded focus:outline-none focus:border-indigo-500" type="username" name="username" value="{{ old('username') }}" />
                </div>
                <div class="mb-3">
                    <label class="block text-gray-700 text-sm font-bold mb-2">Password <span class="text-danger">*</span></label>
                    <input class="w-full border border-gray-300 px-3 py-2 rounded focus:outline-none focus:border-indigo-500" type="password" name="password" />
                </div>
                <div class="mb-3">
                    <label class="block text-gray-700 text-sm font-bold mb-2">Password Confirmation <span class="text-danger">*</span></label>
                    <input class="w-full border border-gray-300 px-3 py-2 rounded focus:outline-none focus:border-indigo-500" type="password" name="password_confirm" />
                </div>
                <div class="mb-3 text-center">
                    <button class="bg-blue-500 text-white px-4 py-2 rounded">Register</button>
                </div>
            </form>

            <p class="mb-5 text-sm font-medium text-center text-gray-700">
                Already have an account? <a href="{{ route('login') }}" class="text-blue-500 hover:underline">Log in</a>
            </p>
        </div>
        @endsection
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
</body>

</html>