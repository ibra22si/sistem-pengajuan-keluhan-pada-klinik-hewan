<!DOCTYPE html>
<html lang="en" class="scroll-smooth group" data-sidebar="brand" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>Login Admin</title>
        <meta  name="viewport"  content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta  content="Tailwind Multipurpose Admin & Dashboard Template"  name="description"/>
        <meta content="" name="Mannatthemes" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" />
        
        <!-- Css -->
        <!-- Main Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/icofont/icofont.min.css">
        <link href="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/css/tailwind.min.css">

    </head>
    
    <body data-layout-mode="light"  data-sidebar-size="default" data-theme-layout="vertical" class="bg-[#EEF0FC] dark:bg-gray-900">
    
    <div class="relative flex flex-col justify-center min-h-screen overflow-hidden">
    <div class="w-full m-auto bg-white dark:bg-slate-800/60 rounded shadow-lg ring-2 ring-slate-300/50 dark:ring-slate-700/50 lg:max-w-md">
        <div class="text-center p-6 bg-slate-900 rounded-t">
                <img src="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" alt="" class="w-14 h-14 mx-auto mb-2">
            </a>
            <h3 class="font-semibold text-white text-xl mb-1">SISTEM PENGAJUAN KELUHAN PADA KLINIK HEWAN</h3>
        </div>

        <form method="POST" action="{{ route('login') }}" class="p-6">
            @csrf
            <div class="mb-4">
                <label for="email" class="font-medium text-sm text-slate-600 dark:text-slate-400">Email</label>
                <input id="email" type="email" class="form-input @error('email') is-invalid @enderror w-full rounded-md mt-1 border border-slate-300/60 dark:border-slate-700 dark:text-slate-300 bg-transparent px-3 py-2 focus:outline-none focus:ring-0 placeholder:text-slate-400/70 placeholder:font-normal placeholder:text-sm hover:border-slate-400 focus:border-primary-500 dark:focus:border-primary-500 dark:hover:border-slate-700" name="email" placeholder="Your Email" required>
            </div>
            <div class="mb-4">
                <label for="password" class="font-medium text-sm text-slate-600 dark:text-slate-400">Your password</label>
                <input id="password" type="password" name="password" required autocomplete="current-password" class="form-input w-full rounded-md mt-1 border border-slate-300/60 dark:border-slate-700 dark:text-slate-300 bg-transparent px-3 py-2 focus:outline-none focus:ring-0 placeholder:text-slate-400/70 placeholder:font-normal placeholder:text-sm hover:border-slate-400 focus:border-primary-500 dark:focus:border-primary-500 dark:hover:border-slate-700" placeholder="Password">
            </div>
            <div class="mb-4">
                <label class="custom-label block dark:text-slate-300">
                    <div class="bg-white dark:bg-slate-700 border border-slate-200 dark:border-slate-600 rounded w-4 h-4 inline-block leading-4 text-center -mb-[3px]">
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">{{ __('Remember Me') }}</label>
                    </div>
                </label>
            </div>
            <div>
                <button type="submit" class="w-full px-2 py-2 tracking-wide text-white transition-colors duration-200 transform bg-brand-500 rounded hover:bg-brand-600 focus:outline-none focus:bg-brand-600">
                    Login
                </button>
            </div>
        </form>
    </div>
</div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        @if (session()->has('success'))
            toastr.success('{{ session('success') }}', 'BERHASIL!');
        @elseif (session()->has('error'))
            toastr.error('{{ session('error') }}', 'GAGAL!');
        @endif
    </script>
</body>

</html>