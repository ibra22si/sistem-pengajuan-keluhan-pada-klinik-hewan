<!DOCTYPE html>
<html lang="en" class="scroll-smooth group" data-sidebar="brand" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>Login</title>
        <meta  name="viewport"  content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta  content="Tailwind Multipurpose Admin & Dashboard Template"  name="description"/>
        <meta content="" name="Mannatthemes" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" />
        
        <!-- Css -->
        <!-- Main Css -->
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/libs/icofont/icofont.min.css">
        <link href="{{asset('AdminLTE')}}/dist/assets/libs/flatpickr/flatpickr.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('AdminLTE')}}/dist/assets/css/tailwind.min.css">

    </head>
    
    <body data-layout-mode="light"  data-sidebar-size="default" data-theme-layout="vertical" class="bg-[#EEF0FC] dark:bg-gray-900">
    
    <div class="relative flex flex-col justify-center min-h-screen overflow-hidden">
    <div class="w-full m-auto bg-white dark:bg-slate-800/60 rounded shadow-lg ring-2 ring-slate-300/50 dark:ring-slate-700/50 lg:max-w-md">
        <div class="text-center p-6 bg-slate-900 rounded-t">
            <img src="https://2.bp.blogspot.com/-sUo24r8Z3Rw/WgLFF_KaNtI/AAAAAAAAEno/dpjiPTiRR2ImWpjJoKUzrke0wFXmWJWKQCLcBGAs/s1600/riau.png" alt="" class="w-14 h-14 mx-auto mb-2">
            <h3 class="font-semibold text-white text-xl mb-1">SISTEM PENGAJUAN KELUHAN PADA KLINIK HEWAN</h3>
        </div>

        <div class="p-6">
        <a href="{{ route('google.redirect') }}" class="block w-full text-center py-3 mb-3 bg-primary text-white rounded-md hover:bg-primary-500">Login</a>

        </div>
    </div>
</div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        @if (session()->has('success'))
            toastr.success('{{ session('success') }}', 'BERHASIL!');
        @elseif (session()->has('error'))
            toastr.error('{{ session('error') }}', 'GAGAL!');
        @endif
    </script>
</body>

</html>