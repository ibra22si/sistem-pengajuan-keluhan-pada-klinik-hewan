<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\keluhan;
use App\Models\pendaftaran;
use App\Models\poli;
use App\Models\emergency;
use App\Models\laporan;


use DB;


class EmergencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emergency = Emergency::with('keluhan')
        ->orderByDesc('id_emergency')->whereNull('status')
        ->get();

    return view('emergency', ['emergency' => $emergency]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'labor',
            'ruangan',
            'id_keluhan'  
        ]);

        $id_keluhan = $request->input('id_keluhan');

        $id_emergency = DB::table('emergency')->insertGetId([
            'labor' => $request->input('labor'),
            'ruangan' => $request->input('ruangan'),
            'id_keluhan' => $id_keluhan,
        ]);

        $latestemergency = DB::table('emergency')
        ->where('id_emergency', $id_emergency)
        ->orderByDesc('id_emergency')
        ->first();

        if ($id_emergency) {
            DB::table('keluhan')
                ->where('id_keluhan', $id_keluhan)
                ->update(['status' => 'emergency']);
        }
   
    return redirect()->route('emergency.index')->with(['success'=> 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $emergency = Poli::find($id);

        if (!$emergency) {
            // Lakukan sesuatu jika data poli tidak ditemukan
        }

        // Mengakses informasi Pendaftaran melalui relasi
        $namaPemilik = $emergency->keluhan->pendaftaran->nama_pemilik;
        $namaHewan = $emergency->keluhan->pendaftaran->nama_hewan;
        $jenisHewan = $emergency->keluhan->pendaftaran->jenis_hewan;

        // Gunakan informasi ini sesuai kebutuhan dalam logika aplikasi Anda
        // ...

        return view('emergency.show', [
            'emergency' => $emergency,
            'namaPemilik' => $namaPemilik,
            'namaHewan' => $namaHewan,
            'jenisHewan' => $jenisHewan,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
