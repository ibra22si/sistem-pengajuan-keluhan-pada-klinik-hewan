<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\keluhan;
use App\Models\pendaftaran;
use App\Models\poli;
use App\Models\emergency;
use App\Models\laporan;
use Illuminate\Support\Facades\Auth;


use DB;

class PemilikHewanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUserId = Auth::id();

        $pendaftaran = DB::select(DB::raw("SELECT * FROM pendaftaran WHERE id_user = :authUserId"), ['authUserId' => $authUserId]);
       return view('pendaftaran.index', compact('pendaftaran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_pemilik',
            'email',
            'alamat',
            'no_hp',
            'no_ktp',
            'nama_hewan',
            'jenis_hewan',
            'usia_hewan',
            'jenis_kelamin',
            'tanggal_daftar',
        ]);
        

        $image = $request->file('foto');
        $image->storeAs('public/pendaftaran', $image->hashName());

        DB::insert(
            "INSERT INTO `pendaftaran` 
            (`id_daftar`, `foto`, `nama_pemilik`, `email`, `alamat`, `no_hp`, `no_ktp`, `nama_hewan`, `jenis_hewan`, `usia_hewan`, `jenis_kelamin`, `tanggal_daftar`, `id_user`) 
            VALUES (uuid(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [
                $image->hashName(),
                $request->nama_pemilik,
                $request->email,
                $request->alamat,
                $request->no_hp,
                $request->no_ktp,
                $request->nama_hewan,
                $request->jenis_hewan,
                $request->usia_hewan,
                $request->jenis_kelamin,
                $request->tanggal_daftar,
                Auth::id()
            ]
        );
        
        return redirect()->route('pendaftaran.index')->with(['success'=> 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
{
    $keluhanToDelete = DB::table('keluhan')->where('id_daftar', $id)->get();

    if($keluhanToDelete->isNotEmpty()) {
        foreach($keluhanToDelete as $keluhan) {
            DB::table('keluhan')->where('id_keluhan', $keluhan->id_keluhan)->delete();
        }
    }

    DB::table('pendaftaran')->where('id_daftar', $id)->delete();

    return redirect()->route('pendaftaran.index')->with(['success' => 'Data Behasil Di Hapus']);
}

 
}
