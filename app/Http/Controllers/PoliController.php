<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\keluhan;
use App\Models\pendaftaran;
use App\Models\poli;
use App\Models\emergency;
use App\Models\laporan;

use DB;


class PoliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $poli = Poli::with('keluhan')
        ->orderByDesc('id_poli')->whereNull('status')
        ->get();

    return view('poli', ['poli' => $poli]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
{
    $this->validate($request, [
        'labor',
        'id_keluhan'  
    ]);

    $id_keluhan = $request->input('id_keluhan');

    // Simpan data ke tabel Poli
    $id_poli = DB::table('poli')->insertGetId([
        'labor' => $request->input('labor'),
        'id_keluhan' => $id_keluhan,
    ]);

    if ($id_poli) {
        DB::table('keluhan')
            ->where('id_keluhan', $id_keluhan)
            ->update(['status' => 'poli']);
    }

    

    // Redirect ke halaman Poli
    return redirect()->route('poli.index')->with(['success'=> 'Data Berhasil Disimpan!']);
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $poli = Poli::find($id);

        if (!$poli) {
            // Lakukan sesuatu jika data poli tidak ditemukan
        }

        // Mengakses informasi Pendaftaran melalui relasi
        $namaPemilik = $poli->keluhan->pendaftaran->nama_pemilik;
        $namaHewan = $poli->keluhan->pendaftaran->nama_hewan;
        $jenisHewan = $poli->keluhan->pendaftaran->jenis_hewan;

        // Gunakan informasi ini sesuai kebutuhan dalam logika aplikasi Anda
        // ...

        return view('poli.show', [
            'poli' => $poli,
            'namaPemilik' => $namaPemilik,
            'namaHewan' => $namaHewan,
            'jenisHewan' => $jenisHewan,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
{   
    $poli = poli::find($id);
   
        if (!$poli) {
            return redirect()->route('poli.index')->with(['error' => 'Keluhan tidak ditemukan']);
        }
   
        // Menghapus data keluhan terkait
        $id_keluhan = $poli->id_keluhan;
        Keluhan::where('id_keluhan', $id_keluhan)->delete();

        // Menghapus poli
        $poli->delete();
   
        return redirect()->route('poli.index')->with(['success' => 'Data Berhasil Di Hapus']);
    }
}
