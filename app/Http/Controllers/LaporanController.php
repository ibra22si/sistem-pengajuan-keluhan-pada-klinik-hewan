<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\keluhan;
use App\Models\pendaftaran;
use App\Models\poli;
use App\Models\emergency;
use App\Models\laporan;
use DB;


class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laporan = Laporan::with('poli', 'emergency')
                    ->orderByDesc('id_laporan')
                    ->get();
    
        return view('laporan', ['laporan' => $laporan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
{
    $this->validate($request, [
        'penyakit',
        'obat',
        'pembayaran',
        'id_poli',
        'id_emergency',
    ]);

    $id_poli = $request->input('id_poli');
    $id_emergency = $request->input('id_emergency');

    $id_laporan = DB::table('laporan')->insertGetId([
        'penyakit' => $request->input('penyakit'),
        'obat' => $request->input('obat'),
        'pembayaran' => $request->input('pembayaran'),
        'id_poli' => $id_poli,
        'id_emergency' => $id_emergency,
    ]);

    if ($id_laporan) {
        // Update status di tabel poli jika id_poli tidak null
        if ($id_poli) {
            DB::table('poli')
                ->where('id_poli', $id_poli)
                ->update(['status' => 'pulang']);
        }
        
        // Update status di tabel laporan jika id_emergency tidak null
        if ($id_emergency) {
            DB::table('emergency')
                ->where('id_emergency', $id_emergency)
                ->update(['status' => 'pulang']);
        }
    }

    return redirect()->route('laporan.index')->with(['success'=> 'Data Berhasil Disimpan!']);
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
{
    $laporan = Laporan::find($id);

    if (!$laporan) {
        return redirect()->route('laporan.index')->with(['error' => 'Keluhan tidak ditemukan']);
    }

    // Menghapus data poli terkait
    $id_poli = $laporan->id_poli;
    $poli = Poli::find($id_poli);
    if ($poli) {
        $poli->delete();
    }

    // Menghapus data emergency terkait
    $id_emergency = $laporan->id_emergency;
    $emergency = Emergency::find($id_emergency);
    if ($emergency) {
        $emergency->delete();
    }

    // Menghapus laporan
    $laporan->delete();

    return redirect()->route('laporan.index')->with(['success' => 'Data Berhasil Di Hapus']);
}

}
