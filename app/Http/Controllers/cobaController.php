<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class cobaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data=DB::select(DB::raw("select * from pendaftaran"));
       return view('coba', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pendaftaran.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_pemilik',
            'email',
            'alamat',
            'no_hp',
            'no_ktp',
            'nama_hewan',
            'jenis_hewan',
            'usia_hewan',
            'tanggal_daftar',
        ]);
        

        $image = $request->file('foto');
        $image->storeAs('public/pendaftaran', $image->hashName());

        DB::insert("INSERT INTO `pendaftaran` (`id_daftar`, `foto`, `nama_pemilik`, `email`, `alamat`, `no_hp`, `no_ktp`, `nama_hewan`, `jenis_hewan`,`usia_hewan`, `tanggal_daftar`) VALUES (uuid(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
        [$image->hashName(),$request->nama_pemilik, $request->email, $request->alamat, $request->no_hp, $request->no_ktp, $request->nama_hewan, $request->jenis_hewan, $request->usia_hewan, $request->tanggal_daftar]);
        return redirect()->route('pendaftaran.index')->with(['success'=> 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=DB::table('pendaftaran')->where('id_daftar', $id)->first();
        return view('pendaftaran.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('pendaftaran')->where('id_daftar', $id)->Delete();
        return redirect()->route('pendaftaran.index')->with(['success' => 'Data Behasil Di Hapus']);
    }
}
