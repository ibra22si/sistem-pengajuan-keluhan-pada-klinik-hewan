<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\keluhan;
use App\Models\pendaftaran;
use App\Models\poli;
use App\Models\emergency;
use App\Models\laporan;

use DB;


class KeluhanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keluhan = Keluhan::with('pendaftaran')
        ->orderByDesc('id_keluhan')->whereNull('status')
        ->get();

    return view('keluhan.utama', ['keluhan' => $keluhan]);
    }
   


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request)
{
    $this->validate($request, [
        'desc_keluhan',
        'tanggal_keluhan',
        'id_daftar'  
    ]);

    $id_daftar = $request->input('id_daftar');

    $id_keluhan = DB::table('keluhan')->insertGetId([
        'desc_keluhan' => $request->input('desc_keluhan'),
        'tanggal_keluhan' => $request->input('tanggal_keluhan'),
        'id_daftar' => $id_daftar,
    ]);

    $latestKeluhan = DB::table('keluhan')
        ->where('id_keluhan', $id_keluhan)
        ->orderByDesc('id_keluhan')
        ->first();
   
    return redirect()->route('keluhan.index')->with(['success'=> 'Data Berhasil Disimpan!']);
}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($id)
        {
            DB::table('keluhan')->where('id_keluhan', $id)->Delete();
            return redirect()->route('keluhan.index')->with(['success' => 'Data Behasil Di Hapus']);
        }
    
}