<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(){
        $data = DB::SELECT(DB::raw("SELECT COUNT(*) as Terdaftar from pendaftaran"));
        $data2 = DB::SELECT(DB::raw("SELECT COUNT(*) as poli from poli"));
        $data3 = DB::SELECT(DB::raw("SELECT COUNT(*) as emergency from emergency"));
        $data4 = DB::SELECT(DB::raw("SELECT COUNT(*) as Pulang from laporan"));
        return view('Dashboard', compact('data', 'data2', 'data3', 'data4'));
    }
}
