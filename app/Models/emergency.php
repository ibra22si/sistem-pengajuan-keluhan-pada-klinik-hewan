<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class emergency extends Model
{
    protected $table = 'emergency'; 
    protected $primaryKey = 'id_emergency'; 
    
    public function keluhan() {
        return $this->belongsTo(Keluhan::class, 'id_keluhan');
    }

    protected static function booted() {
        static::deleting(function ($emergency) {
            // Menghapus keluhan terkait saat emergency dihapus
            $emergency->keluhan()->delete();
        });
    }

    public function laporan()
    {
        return $this->hasMany(Laporan::class, 'id_emergency');
    }

}
