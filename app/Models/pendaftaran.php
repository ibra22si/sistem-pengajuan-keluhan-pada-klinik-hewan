<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Pendaftaran extends Model
{
    protected $table = 'pendaftaran';
    
    public function pendaftaran() 
    {
        return $this->belongsTo(Pendaftaran::class, 'id_daftar', 'id_daftar');
    }
    public function user() 
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }
}

