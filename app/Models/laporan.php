<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laporan extends Model
{
    protected $table = 'laporan';
    protected $primaryKey = 'id_laporan';

    public function poli() 
    {
        return $this->belongsTo(Poli::class, 'id_poli');
    }

    public function emergency() 
    {
        return $this->belongsTo(Emergency::class, 'id_emergency');
    }
    public function user() 
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }
    
}

