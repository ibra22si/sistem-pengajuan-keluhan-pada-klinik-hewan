<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keluhan extends Model
{
    protected $table = 'keluhan';
    protected $primaryKey = 'id_keluhan';
   
    public function pendaftaran() 
    {
        return $this->belongsTo(Pendaftaran::class, 'id_daftar', 'id_daftar');
    }

    protected static function booted()
    {
        // Event deleting pada model Keluhan
        static::deleting(function ($keluhan) {
            // Menghapus data dari tabel Pendaftaran yang memiliki foreign key yang sama dengan id_daftar pada Keluhan yang dihapus
            Pendaftaran::where('id_daftar', $keluhan->id_daftar)->delete();
        });
    }


    public function poli()
    {
        return $this->hasMany(Poli::class, 'id_keluhan');
    }

    public function emergency()
    {
        return $this->hasMany(emergency::class, 'id_keluhan');
    }
}

