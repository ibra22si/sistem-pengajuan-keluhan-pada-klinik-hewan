<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class poli extends Model
{
    protected $table = 'poli'; 
    protected $primaryKey = 'id_poli'; 
    
    public function keluhan() {
        return $this->belongsTo(Keluhan::class, 'id_keluhan');
    }

    protected static function booted() {
        static::deleting(function ($poli) {
            // Menghapus keluhan terkait saat poli dihapus
            $poli->keluhan()->delete();
        });
    }

    public function laporan()
    {
        return $this->hasMany(Poli::class, 'id_poli');
    }
}

