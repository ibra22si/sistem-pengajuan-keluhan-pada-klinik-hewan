<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KeluhanController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('User.Home');
});

Route::get('/loginUser', function () {
    return view('auth.loginGoogle');
});

Route::get('/loginUser', [App\Http\Controllers\GoogleLoginController::class, 'login'])->name('google.login');

Route::get('/tambah', function () {
    return view('keluhan.tambah');
});

Route::resource('/keluhan',
\App\Http\Controllers\KeluhanController::class);

Route::resource('/Dashboard', '\App\Http\Controllers\DashboardController')
    ->middleware('auth', 'restrict.user:null'); // Change 'null' to the role allowed to access

Route::resource('/DataPasien', '\App\Http\Controllers\DataPasienController')
    ->middleware('auth', 'restrict.user:null');

Route::resource('/emergency', '\App\Http\Controllers\EmergencyController')
    ->middleware('auth', 'restrict.user:null');


Route::get('/google/redirect', [App\Http\Controllers\GoogleLoginController::class, 'redirectToGoogle'])->name('google.redirect');
Route::get('/google/callback', [App\Http\Controllers\GoogleLoginController::class, 'handleGoogleCallback'])->name('google.callback');

Route::resource('/poli',
\App\Http\Controllers\PoliController::class)->middleware('auth');


Route::resource('/laporan',
\App\Http\Controllers\LaporanController::class)->middleware('auth');

Route::resource('/pendaftaran',
\App\Http\Controllers\PemilikHewanController::class);

Route::resource('/HewanPulang',
\App\Http\Controllers\RiwayatHewanController::class);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::resource('/coba',
\App\Http\Controllers\cobaController::class);


Route::resource('/ViewAdmin',
\App\Http\Controllers\AdminController::class);

Route::resource('/Home',
\App\Http\Controllers\HomeController::class);

Route::delete('/keluhan/{id_keluhan}', 'KeluhanController@hapusKeluhan')->name('keluhan.hapusKeluhan');